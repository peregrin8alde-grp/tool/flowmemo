= フロントエンド GUI

以下を利用して GUI となる html を作成する。

* https://ja.vuejs.org/[Vue.js]
* https://ja.vitejs.dev/[Vite]

デザインの設計ではアトミックデザインの考え方を参考にする。
（ Templates や Pages から作って、必要に応じて部品化していく）

バックエンドとの通信用 API は OpenAPI ベースの仕様書を元に自動生成されたコードを使って開発する。

* https://openapi-generator.tech/[OpenAPI Generator] を利用
** https://openapi-generator.tech/docs/generators/typescript-fetch/[typescript-fetch]

markdown で表示

* https://github.com/markedjs/marked[Marked] を利用
* エディタとして https://github.com/Ionaru/easy-markdown-editor[EasyMDE]
