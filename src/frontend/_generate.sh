#!/bin/sh

SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE:-$0}")" && pwd)
PARENT_DIR=$(cd "$(dirname "${BASH_SOURCE:-$0}")/.." && pwd)

OPENAPI_DIR=$(cd "$(dirname "${BASH_SOURCE:-$0}")/../api" && pwd)

APP_NAME=flowmemo-cl
OUTDIR="${SCRIPT_DIR}/${APP_NAME}/src/openapi/generated"

rm -rf "${OUTDIR}"
mkdir -p "${OUTDIR}"

docker run \
  --rm \
  --user 1000:1000 \
  -v "${OPENAPI_DIR}":/usr/src/yaml \
  -v "${OUTDIR}":/usr/src/generated \
  -w /usr/src/generated \
  openapitools/openapi-generator-cli \
    generate \
    -i /usr/src/yaml/memostore.yaml \
    -g typescript-fetch \
    -o /usr/src/generated



exit 0
