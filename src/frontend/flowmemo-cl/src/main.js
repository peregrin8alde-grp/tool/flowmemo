import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import "bootstrap/dist/css/bootstrap.min.css"
import "easymde/dist/easymde.min.css"

createApp(App).mount('#app')
