#!/bin/sh

SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE:-$0}")" && pwd)
PARENT_DIR=$(cd "$(dirname "${BASH_SOURCE:-$0}")/.." && pwd)

APP_NAME=flowmemo-cl

# npm は Docker コンテナ利用
npm () {
  docker run \
    --rm \
    -u node \
    -it \
    --name ${APP_NAME} \
    -v npm_modules:/usr/local/lib/node_modules \
    -v "$(pwd):$(pwd)" \
    -w $(pwd) \
    node \
      npm "$@"
  
  return 0
}


# run
cd ${SCRIPT_DIR}/${APP_NAME}
npm run build -- --base=/public





exit 0
