#!/bin/sh

SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE:-$0}")" && pwd)
PARENT_DIR=$(cd "$(dirname "${BASH_SOURCE:-$0}")/.." && pwd)

APP_NAME=flowmemo-cl

# npm は Docker コンテナ利用
npm () {
  docker run \
    --rm \
    -u node \
    -v npm_modules:/usr/local/lib/node_modules \
    -v "$(pwd):$(pwd)" \
    -w $(pwd) \
    node \
      npm "$@"
  
  return 0
}

# 初期化
cd ${SCRIPT_DIR}
npm create vite@latest ${APP_NAME} -- --template vue

# install
cd ${APP_NAME}
npm install

npm install bootstrap @popperjs/core
npm install marked
npm install easymde


exit 0
