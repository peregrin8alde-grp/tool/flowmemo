package models

type AppConfig struct {
	StorageType *string `json:"storageType,omitempty"`
	InitializeMode *bool `json:"initializemode,omitempty"`
	InitializeSrcStorageType *string `json:"initializesrcstoragetype,omitempty"`
}
