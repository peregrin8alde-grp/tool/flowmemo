#!/bin/sh

SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE:-$0}")" && pwd)
PARENT_DIR=$(cd "$(dirname "${BASH_SOURCE:-$0}")/.." && pwd)


docker run \
  --rm \
  -v gopath:/go \
  -v "${SCRIPT_DIR}":/usr/src/myapp \
  -w /usr/src/myapp \
  golang:1.20 \
    go mod tidy


docker run \
  --rm \
  -it \
  --name flowmemo_websv \
  -p 1323:1323 \
  -v gopath:/go \
  -v "${SCRIPT_DIR}":/usr/src/myapp \
  -w /usr/src/myapp \
  golang:1.20 \
    go run main.go




exit 0
