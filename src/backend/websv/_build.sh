#!/bin/sh

SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE:-$0}")" && pwd)
PARENT_DIR=$(cd "$(dirname "${BASH_SOURCE:-$0}")/.." && pwd)


rm -rf "${SCRIPT_DIR}/bin/"
mkdir -p "${SCRIPT_DIR}/bin/"

docker run \
  --rm \
  -v gopath:/go \
  -v "${SCRIPT_DIR}":/usr/src/myapp \
  -w /usr/src/myapp \
  golang:1.20 \
    go mod tidy


docker run \
  --rm \
  -v gopath:/go \
  -v "${SCRIPT_DIR}":/usr/src/myapp \
  -w /usr/src/myapp \
  -e GOOS=windows \
  -e GOARCH=386 \
  x1unix/go-mingw:latest \
    go build -v -o bin/




exit 0
