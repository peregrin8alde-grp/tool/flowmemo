package api

import (
	"fmt"
	"net/http"
	"time"
    "os"
    "log"
    "encoding/json"
    "io"
    "strconv"

	"github.com/labstack/echo/v4"
	. "websv/models"
	"websv/storage"
)

// ServerInterface の実装用構造体
// 実装メソッドの方で使われるまで ServerInterface の実装だということは分からない？
//   func (変数名 構造体名) 関数名 ～ で構造体と実装メソッドが関連付けられる
type ServerImpl struct {
	Storage StorageInterface
    BaseDir string
}

func NewServer(dir string) *ServerImpl {
	var appConfig AppConfig
    appConfigFile, err := os.Open("config/appconfig.json")
    if err != nil {
        log.Print(err)
    }
    dec := json.NewDecoder(appConfigFile)
    if err = dec.Decode(&appConfig); err != nil {
        log.Print(err)
    }
    appConfigFile.Close()
	if appConfig.StorageType != nil {
		log.Printf("%s", *appConfig.StorageType)
	}
	if appConfig.InitializeMode != nil {
		log.Printf("%t", *appConfig.InitializeMode)
	}
	if appConfig.InitializeSrcStorageType != nil {
		log.Printf("%s", *appConfig.InitializeSrcStorageType)
	}

	
	if err = os.MkdirAll(dir, 0755); err != nil {
		log.Print(err)
	}

	if f, err := os.Stat(dir + "/nextId.txt"); os.IsNotExist(err) || f.IsDir() {

		nextIdStr := "1"
        nextIdFile, err := os.Create(dir + "/nextId.txt")
        if err != nil {
            log.Print(err)
        }
        if _, err := io.WriteString(nextIdFile, nextIdStr); err != nil {
            log.Print(err)
        }
        nextIdFile.Close()
    }

	var memoStorage StorageInterface
	if appConfig.StorageType == nil {
		memoStorage = storage.NewMemoryStorage()
	} else if *appConfig.StorageType == "file" {
		memoStorage = storage.NewFileStorage("data")
	} else if *appConfig.StorageType == "memory" {
		memoStorage = storage.NewMemoryStorage()
	} else if *appConfig.StorageType == "sqlite" {
		memoStorage = storage.NewSqliteStorage("db")
	} else {
		log.Printf("StorageType is invalid : %s", *appConfig.StorageType)
		// 本来はエラーか、警告出してデフォルト動作
		memoStorage = storage.NewMemoryStorage()
	}

	if appConfig.InitializeMode == nil {
		// 何もしない
	} else if *appConfig.InitializeMode == true {
		// 別タイプのストレージにあるデータを使って初期化
		var srcMemoStorage StorageInterface
		var params FindMemosParams
		
		if appConfig.InitializeSrcStorageType == nil {
			srcMemoStorage = storage.NewFileStorage("data")
		} else if *appConfig.InitializeSrcStorageType == "file" {
			srcMemoStorage = storage.NewFileStorage("data")
		} else if *appConfig.InitializeSrcStorageType == "sqlite" {
			srcMemoStorage = storage.NewSqliteStorage("db")
		} else {
			log.Printf("InitializeSrcStorageType is invalid : %s", *appConfig.InitializeSrcStorageType)
			// 本来はエラーか、警告出してデフォルト動作
		}

		srcMemos, err := srcMemoStorage.SelectMemos(params)
		if err != nil {
			log.Printf("srcMemoStorage.SelectMemos is faild")
			// 本来はエラーか、警告出してデフォルト動作
		}

		err = memoStorage.SaveMemos(srcMemos)
		if err != nil {
			log.Printf("memoStorage.SaveMemos is faild")
			// 本来はエラーか、警告出してデフォルト動作
		}
	} else if *appConfig.InitializeMode == false {
		// 何もしない
	}
	
	return &ServerImpl{
		Storage: memoStorage,
        BaseDir: dir,
	}
}

func sendServerError(ctx echo.Context, code int, message string) error {
	memoErr := Error{
		Code:    int32(code),
		Message: message,
	}
	err := ctx.JSON(code, memoErr)
	return err
}

func (sv *ServerImpl) FindMemos(ctx echo.Context, params FindMemosParams) error {
	result, err := sv.Storage.SelectMemos(params)
	if err != nil {
		return sendServerError(ctx, http.StatusBadRequest, "Internal Server Error")
	}

    return ctx.JSON(http.StatusOK, result)
}

func (sv *ServerImpl) AddMemo(ctx echo.Context) error {
	var newMemo NewMemo
	err := ctx.Bind(&newMemo)
	if err != nil {
		return sendServerError(ctx, http.StatusBadRequest, "Invalid format for NewMemo")
	}

	var memo Memo
	memo.Id, err = sv.GenarateId()
	if err != nil {
		return sendServerError(ctx, http.StatusBadRequest, "Internal Server Error")
	}
	memo.Meta.CreatedAt = time.Now().UnixMilli()
	memo.Meta.UpdatedAt = memo.Meta.CreatedAt
	memo.Text = newMemo.Text
	memo.Tag = newMemo.Tag

	err = sv.Storage.WriteMemo(memo)
	if err != nil {
		return sendServerError(ctx, http.StatusBadRequest, "Internal Server Error")
	}

	err = ctx.JSON(http.StatusCreated, memo)
	if err != nil {
		return err
	}


    return nil
}

func (sv *ServerImpl) DeleteMemo(ctx echo.Context, id int64) error {
	_, err := sv.Storage.ReadMemo(id)
	if err != nil {
		return sendServerError(ctx, http.StatusNotFound,
			fmt.Sprintf("Could not find memo with ID %d", id))
	}

    err = sv.Storage.DeleteMemo(id)
	if err != nil {
		return sendServerError(ctx, http.StatusBadRequest, "Internal Server Error")
	}

	return ctx.NoContent(http.StatusNoContent)
}

func (sv *ServerImpl) FindMemoByID(ctx echo.Context, id int64) error {
	memo, err := sv.Storage.ReadMemo(id)
	if err != nil {
		return sendServerError(ctx, http.StatusNotFound,
			fmt.Sprintf("Could not find memo with ID %d", id))
	}

    return ctx.JSON(http.StatusOK, memo)
}


func (sv *ServerImpl) UpdateMemo(ctx echo.Context, id int64) error {
	var newMemo NewMemo
	err := ctx.Bind(&newMemo)
	if err != nil {
		return sendServerError(ctx, http.StatusBadRequest, "Invalid format for NewMemo")
	}

	memo, err := sv.Storage.ReadMemo(id)
	if err != nil {
		return sendServerError(ctx, http.StatusNotFound,
			fmt.Sprintf("Could not find memo with ID %d", id))
	}

	memo.Meta.UpdatedAt = time.Now().UnixMilli()
	memo.Text = newMemo.Text
	memo.Tag = newMemo.Tag

	err = sv.Storage.WriteMemo(memo)
	if err != nil {
		return sendServerError(ctx, http.StatusBadRequest, "Internal Server Error")
	}


    return ctx.JSON(http.StatusOK, memo)
}


func (sv *ServerImpl) GenarateId() (int64, error) {
    log.Print("NextId")

    nextIdFile, err := os.Open(sv.BaseDir + "/nextId.txt")
    if err != nil {
        log.Print(err)
        return 0, err
    }
    data, err := io.ReadAll(nextIdFile)
    if err != nil {
        log.Print(err)
        return 0, err
    }
    nextIdFile.Close()

    nextIdStr := string(data)
    nextId, err := strconv.ParseInt(nextIdStr, 10, 64)
    if err != nil {
        log.Print(err)
        return 0, err
    }

    nextIdStr = strconv.FormatInt(nextId + 1, 10)
    nextIdFile, err = os.Create(sv.BaseDir + "/nextId.txt")
    if err != nil {
        log.Print(err)
        return 0, err
    }
    if _, err := io.WriteString(nextIdFile, nextIdStr); err != nil {
		log.Print(err)
        return 0, err
	}
    nextIdFile.Close()


    return nextId, nil
}
