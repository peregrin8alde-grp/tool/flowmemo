package api

import (
	. "websv/models"
)

type StorageInterface interface {
	SelectMemos(params FindMemosParams) ([]Memo, error)
	WriteMemo(memo Memo) error
	ReadMemo(id int64) (Memo, error)
	DeleteMemo(id int64) error
	SaveMemos(memos []Memo) error
}
