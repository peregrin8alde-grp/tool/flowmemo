#!/bin/sh

SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE:-$0}")" && pwd)
PARENT_DIR=$(cd "$(dirname "${BASH_SOURCE:-$0}")/.." && pwd)

docker run \
  --rm \
  -v gopath:/go \
  golang:1.20 \
    go install github.com/deepmap/oapi-codegen/cmd/oapi-codegen@latest


docker run \
  --rm \
  --user 1000:1000 \
  -v gopath:/go \
  -v "${SCRIPT_DIR}":/usr/src/myapp \
  -w /usr/src/myapp \
  golang:1.20 \
    go mod init websv

mkdir -p "${SCRIPT_DIR}/api"
mkdir -p "${SCRIPT_DIR}/models"
mkdir -p "${SCRIPT_DIR}/server"
mkdir -p "${SCRIPT_DIR}/storage"


docker run \
  --rm \
  -v gopath:/go \
  -v "${SCRIPT_DIR}":/usr/src/myapp \
  -w /usr/src/myapp \
  golang:1.20 \
    go get github.com/labstack/echo/v4

docker run \
  --rm \
  -v gopath:/go \
  -v "${SCRIPT_DIR}":/usr/src/myapp \
  -w /usr/src/myapp \
  golang:1.20 \
    go get github.com/mattn/go-sqlite3



exit 0
