#!/bin/sh

SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE:-$0}")" && pwd)
PARENT_DIR=$(cd "$(dirname "${BASH_SOURCE:-$0}")/.." && pwd)

OPENAPI_DIR=$(cd "$(dirname "${BASH_SOURCE:-$0}")/../../api" && pwd)

# パッケージ関係を以下とするために、インターフェースとモデルのパッケージを分ける（依存関係のサイクルを回避）
## server : API サーバ本体、 API インターフェース。 api パッケージを利用
##          API インターフェースで models パッケージを利用するために公式サンプルを参考に
##          config で additional-imports を使っているが、詳細不明。 template を使うのもあり？
## api : API 実装。 models パッケージを利用
## models : API で使われる各種データ型

# サーバ用 API インターフェース
docker run \
  --rm \
  -it \
  --user 1000:1000 \
  -v gopath:/go \
  -v "${OPENAPI_DIR}":/usr/src/yaml \
  -v "${SCRIPT_DIR}/server":/usr/src/myapp \
  -w /usr/src/myapp \
  golang:1.20 \
    oapi-codegen \
      -config "server.cfg.yaml" \
      /usr/src/yaml/memostore.yaml

docker run \
  --rm \
  -it \
  --user 1000:1000 \
  -v gopath:/go \
  -v "${OPENAPI_DIR}":/usr/src/yaml \
  -v "${SCRIPT_DIR}/models":/usr/src/myapp \
  -w /usr/src/myapp \
  golang:1.20 \
    oapi-codegen \
      -generate types \
      -package models \
      -o "memostore-models.gen.go" \
      /usr/src/yaml/memostore.yaml



exit 0
