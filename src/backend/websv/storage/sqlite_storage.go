package storage

import (
    "os"
    "log"
    "database/sql"
    "strings"
    "fmt"
    _ "github.com/mattn/go-sqlite3"

    . "websv/models"
)

type SqliteStorage struct {
    BaseDir string
    Db *sql.DB
}

func NewSqliteStorage(dir string) *SqliteStorage {
    log.Print("NewSqliteStorage")
    if err := os.MkdirAll(dir, 0755); err != nil {
        log.Print(err)
    }

    db, err := sql.Open("sqlite3", dir + "/data.db")
	if err != nil {
		log.Print(err)
	}
    // この中で使いきるわけではないので、メインから呼び出せる後処理用メソッドを用意したい
	//defer db.Close()

    sqlStmt := `
	CREATE TABLE IF NOT EXISTS text (
        id INTEGER NOT NULL PRIMARY KEY, 
        data TEXT
    );
	CREATE TABLE IF NOT EXISTS meta (
        id INTEGER NOT NULL PRIMARY KEY, 
        created_at INTEGER,
        updated_at INTEGER
    );
	CREATE TABLE IF NOT EXISTS tag (
        id INTEGER NOT NULL PRIMARY KEY, 
        tags TEXT
    );
    `
	_, err = db.Exec(sqlStmt)
	if err != nil {
		log.Printf("%q: %s\n", err, sqlStmt)
	}

    return &SqliteStorage{
        BaseDir: dir,
        Db: db,
    }
}


func (st *SqliteStorage) SelectMemos(params FindMemosParams) ([]Memo, error) {
    log.Print("SelectMemos")

    var result = []Memo{}

    // WHERE 句の構築はもっとうまいやり方考えたい
    query := `
    SELECT
      text.id,
      text.data,
      meta.created_at,
      meta.updated_at,
      tag.tags
    FROM text
      INNER JOIN meta ON text.id = meta.id
      INNER JOIN tag ON text.id = tag.id
    WHERE 1 = 1
	`

    // SQL インジェクション未対応
    if params.Tags != nil {
        tags := ""
        for _, t := range *params.Tags {
            tags += fmt.Sprintf("'%s',", t)
        }
        tags = strings.TrimRight(tags, ",")
        log.Print(tags)

        query += fmt.Sprintf(" AND tag.tags IN (%s)", tags)
    }

    if params.Text != nil {
        log.Print(*params.Text)
        query += fmt.Sprintf(" AND text.data GLOB '%s'", *params.Text)
    }

    if params.Limit != nil {
        query += fmt.Sprintf(" LIMIT %d", *params.Limit)
    }

    // log.Print(query)
    rows, err := st.Db.Query(query)
	if err != nil {
		log.Print(err)
	}
	defer rows.Close()
	for rows.Next() {
        var memo Memo

        var tag sql.NullString
		err = rows.Scan(
            &memo.Id,
            &memo.Text,
            &memo.Meta.CreatedAt,
            &memo.Meta.UpdatedAt,
            &tag,
        )
		if err != nil {
			log.Print(err)
		}

        if tag.Valid {
            memo.Tag = &tag.String
        } else {
            memo.Tag = nil
        }

        result = append(result, memo)
    }
	err = rows.Err()
	if err != nil {
		log.Print(err)
	}

    return result, nil
}

func (st *SqliteStorage) WriteMemo(memo Memo) error {
    log.Print("WriteMemo")

	tx, err := st.Db.Begin()
	if err != nil {
		log.Print(err)
	}

    // text
	stmtText, err := tx.Prepare(`
    INSERT
      INTO text
      (id, data) VALUES (?, ?)
    ON CONFLICT (id)
      DO UPDATE SET data=?
	`)
	if err != nil {
		log.Print(err)
	}
	defer stmtText.Close()
	_, err = stmtText.Exec(memo.Id, memo.Text, memo.Text)
	if err != nil {
		log.Print(err)
	}

    // meta
	stmtMeta, err := tx.Prepare(`
    INSERT
      INTO meta
      (id, created_at, updated_at) VALUES (?, ?, ?)
    ON CONFLICT (id)
      DO UPDATE SET created_at=?, updated_at=?
    `)
	if err != nil {
		log.Print(err)
	}
	defer stmtMeta.Close()
	_, err = stmtMeta.Exec(memo.Id, memo.Meta.CreatedAt, memo.Meta.UpdatedAt, memo.Meta.CreatedAt, memo.Meta.UpdatedAt)
	if err != nil {
		log.Print(err)
	}

    // tag
    if memo.Tag == nil {
        stmtTag, err := tx.Prepare(`
        INSERT
          INTO tag
          (id, tags) VALUES (?, NUll)
        ON CONFLICT (id)
          DO UPDATE SET tags=NUll
        `)
        if err != nil {
            log.Print(err)
        }
        defer stmtTag.Close()
        _, err = stmtTag.Exec(memo.Id)
        if err != nil {
            log.Print(err)
        }
    } else {
        stmtTag, err := tx.Prepare(`
        INSERT
          INTO tag
          (id, tags) VALUES (?, ?)
        ON CONFLICT (id)
          DO UPDATE SET tags=?
        `)
        if err != nil {
            log.Print(err)
        }
        defer stmtTag.Close()
        _, err = stmtTag.Exec(memo.Id, *memo.Tag, *memo.Tag)
        if err != nil {
            log.Print(err)
        }        
    }

	err = tx.Commit()
	if err != nil {
		log.Print(err)
	}


    return nil
}

func (st *SqliteStorage) ReadMemo(id int64) (Memo, error) {
    log.Print("ReadMemo")

    var memo Memo

    query := `
    SELECT
      text.id,
      text.data,
      meta.created_at,
      meta.updated_at,
      tag.tags
    FROM text
      INNER JOIN meta ON text.id = meta.id
      INNER JOIN tag ON text.id = tag.id
    WHERE text.id = ?
	`
    stmt, err := st.Db.Prepare(query)
	if err != nil {
		log.Print(err)
        return memo, err
	}
	defer stmt.Close()

    var tag sql.NullString
	err = stmt.QueryRow(id).Scan(
        &memo.Id,
        &memo.Text,
        &memo.Meta.CreatedAt,
        &memo.Meta.UpdatedAt,
        &tag,
    )
	if err != nil {
		log.Print(err)
        return memo, err
	}

    if tag.Valid {
        memo.Tag = &tag.String
    } else {
        memo.Tag = nil
    }


    return memo, nil
}

func (st *SqliteStorage) DeleteMemo(id int64) error {
    log.Print("DeleteMemo")

	tx, err := st.Db.Begin()
	if err != nil {
		log.Print(err)
	}

    // text
	stmtText, err := tx.Prepare("DELETE FROM text WHERE id = ?")
	if err != nil {
		log.Print(err)
	}
	defer stmtText.Close()
	_, err = stmtText.Exec(id)
	if err != nil {
		log.Print(err)
	}

    // meta
	stmtMeta, err := tx.Prepare("DELETE FROM meta WHERE id = ?")
	if err != nil {
		log.Print(err)
	}
	defer stmtMeta.Close()
	_, err = stmtMeta.Exec(id)
	if err != nil {
		log.Print(err)
	}

    // tag
    stmtTag, err := tx.Prepare("DELETE FROM tag WHERE id = ?")
    if err != nil {
        log.Print(err)
    }
    defer stmtTag.Close()
    _, err = stmtTag.Exec(id)
    if err != nil {
        log.Print(err)
    }

	err = tx.Commit()
	if err != nil {
		log.Print(err)
	}


    return nil
}

func (st *SqliteStorage) SaveMemos(memos []Memo) error {
    log.Print("SaveMemos")

    for _, memo := range memos {
        err := st.WriteMemo(memo)
        if err != nil {
            return err
        }
    }

	return nil
}
