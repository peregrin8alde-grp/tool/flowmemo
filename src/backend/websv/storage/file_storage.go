package storage

import (
    "os"
    "log"
    "strings"
    "io"
    "strconv"
    "encoding/json"
    "regexp"

    . "websv/models"
)

type FileStorage struct {
    BaseDir string
}

func NewFileStorage(dir string) *FileStorage {
    log.Print("NewFileStorage")
    if err := os.MkdirAll(dir + "/meta", 0755); err != nil {
        log.Print(err)
    }
    if err := os.MkdirAll(dir + "/text", 0755); err != nil {
        log.Print(err)
    }
    if err := os.MkdirAll(dir + "/tag", 0755); err != nil {
        log.Print(err)
    }

    return &FileStorage{
        BaseDir: dir,
    }
}


func (st *FileStorage) SelectMemos(params FindMemosParams) ([]Memo, error) {
    log.Print("SelectMemos")

    var result = []Memo{}

    dir_entries, err := os.ReadDir(st.BaseDir + "/meta")
    if err != nil {
        log.Print(err)
        return nil, err
    }

    for _, entry := range dir_entries {
        fileName := entry.Name()
        log.Print(fileName)

        parts := strings.Split(fileName, ".")
        log.Print(parts)

        if len(parts) != 2 {
            continue
        }

        idStr := parts[0]
        id, err := strconv.ParseInt(idStr, 10, 64)
        if err != nil {
            continue
        }

        memo, err := st.ReadMemo(id)
        if err != nil {
            continue
        }

		if params.Tags != nil {
			for _, t := range *params.Tags {
				if memo.Tag != nil && (*memo.Tag == t) {
					result = append(result, memo)
				}
			}
		} else if params.Text != nil {
            reg := regexp.MustCompile(*params.Text)
			if reg.MatchString(memo.Text) {
                result = append(result, memo)
            }
		} else {
			result = append(result, memo)
		}

		if params.Limit != nil {
			l := int(*params.Limit)
			if len(result) >= l {
				// We're at the limit
				break
			}
		}
    }


    return result, nil
}

func (st *FileStorage) WriteMemo(memo Memo) error {
    log.Print("WriteMemo")

    idStr := strconv.FormatInt(memo.Id, 10)

    metaFile, err := os.Create(st.BaseDir + "/meta/" + idStr + ".json")
    if err != nil {
        log.Print(err)
        return err
    }
    enc := json.NewEncoder(metaFile)
    if err := enc.Encode(&(memo.Meta)); err != nil {
		log.Print(err)
        return err
	}
    metaFile.Close()

    dataFile, err := os.Create(st.BaseDir + "/text/" + idStr + ".md")
    if err != nil {
        log.Print(err)
        return err
    }
    if _, err := io.WriteString(dataFile, memo.Text); err != nil {
		log.Print(err)
        return err
	}
    dataFile.Close()

    if memo.Tag != nil {
        tagFile, err := os.Create(st.BaseDir + "/tag/" + idStr + ".txt")
        if err != nil {
            log.Print(err)
            return err
        }
        if _, err := io.WriteString(tagFile, *memo.Tag); err != nil {
            log.Print(err)
            return err
        }
        tagFile.Close()
    }


    return nil
}

func (st *FileStorage) ReadMemo(id int64) (Memo, error) {
    log.Print("ReadMemo")

    var memo Memo
    memo.Id = id

    idStr := strconv.FormatInt(id, 10)

    log.Print("meta")
    metaFile, err := os.Open(st.BaseDir + "/meta/" + idStr + ".json")
    if err != nil {
        log.Print(err)
        return memo, err
    }
    dec := json.NewDecoder(metaFile)
    if err := dec.Decode(&(memo.Meta)); err != nil {
        log.Print(err)
        return memo, err
    }
    metaFile.Close()

    log.Printf("%s", memo.Meta)

    log.Print("data")
    dataFile, err := os.Open(st.BaseDir + "/text/" + idStr + ".md")
    if err != nil {
        log.Print(err)
        return memo, err
    }
    data, err := io.ReadAll(dataFile)
    if err != nil {
        log.Print(err)
        return memo, err
    }
    dataFile.Close()

    log.Printf("%s", data)

    memo.Text = string(data)

    if _, err := os.Stat(st.BaseDir + "/tag/" + idStr + ".txt"); err == nil {
        log.Print("tag")
        tagFile, err := os.Open(st.BaseDir + "/tag/" + idStr + ".txt")
        if err != nil {
            log.Print(err)
            return memo, err
        }
        tag, err := io.ReadAll(tagFile)
        if err != nil {
            log.Print(err)
            return memo, err
        }
        tagFile.Close()
    
        log.Printf("%s", tag)
        tagStr := string(tag)
        memo.Tag = &tagStr
    }


    return memo, nil
}

func (st *FileStorage) DeleteMemo(id int64) error {
    log.Print("DeleteMemo")

    idStr := strconv.FormatInt(id, 10)

    if err := os.Remove(st.BaseDir + "/meta/" + idStr + ".json"); err != nil {
        log.Print(err)
        return err
    }
    if err := os.Remove(st.BaseDir + "/text/" + idStr + ".md"); err != nil {
        log.Print(err)
        return err
    }

    if _, err := os.Stat(st.BaseDir + "/tag/" + idStr + ".txt"); err == nil {
        if err := os.Remove(st.BaseDir + "/tag/" + idStr + ".txt"); err != nil {
            log.Print(err)
            return err
        }
    }


    return nil
}

func (st *FileStorage) SaveMemos(memos []Memo) error {
    log.Print("SaveMemos")

    for _, memo := range memos {
        err := st.WriteMemo(memo)
        if err != nil {
            return err
        }
    }

	return nil
}
