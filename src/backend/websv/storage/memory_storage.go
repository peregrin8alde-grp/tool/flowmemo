package storage

import (
    "log"
	"fmt"

    . "websv/models"
)

type MemoryStorage struct {
    Memos map[int64]Memo
}

func NewMemoryStorage() *MemoryStorage {
    return &MemoryStorage{
        Memos: make(map[int64]Memo),
    }
}

func (st *MemoryStorage) SelectMemos(params FindMemosParams) ([]Memo, error) {
    log.Print("SelectMemos")

    var result = []Memo{}

	for _, memo := range st.Memos {
		if params.Tags != nil {
			for _, t := range *params.Tags {
				if memo.Tag != nil && (*memo.Tag == t) {
					result = append(result, memo)
				}
			}
		} else {
			result = append(result, memo)
		}

		if params.Limit != nil {
			l := int(*params.Limit)
			if len(result) >= l {
				// We're at the limit
				break
			}
		}
    }


    return result, nil
}

func (st *MemoryStorage) WriteMemo(memo Memo) error {
    log.Print("WriteMemo")

	st.Memos[memo.Id] = memo

    return nil
}

func (st *MemoryStorage) ReadMemo(id int64) (Memo, error) {
    log.Print("ReadMemo")

    if memo, ok := st.Memos[id]; ok {
		return memo, nil
	} else {
		return memo, fmt.Errorf("not found: %d", id)
	}
}

func (st *MemoryStorage) DeleteMemo(id int64) error {
    log.Print("DeleteMemo")

	if _, ok := st.Memos[id]; ok {
		delete(st.Memos, id)
		return nil
	} else {
		return fmt.Errorf("not found: %d", id)
	}

}

func (st *MemoryStorage) SaveMemos(memos []Memo) error {
    log.Print("SaveMemos")

    for _, memo := range memos {
        err := st.WriteMemo(memo)
        if err != nil {
            return err
        }
    }


	return nil
}

